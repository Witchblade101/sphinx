###############################
INS Technical Training Library
###############################

.. warning::

   **To non-STScI readers:** These documents are designed for internal use only, and are not an official AURA/STScI publication. Information may be out of date, irrelevant, or contradict published documentation. Use at your own risk!

The Research & Instrument Analysis branch has produced training documents for new hires that are now used for all new technical staff in the Instruments division at STScI. The Instruments Division also offers lectures on important topics (such as instruments, operations, and pipelines) whenever a large cohort of new hires arrives at STScI.

**If you've arrived with a large cohort:** Visit the `New hire training <https://confluence.stsci.edu/display/INSTraining/New+hire+training>`_ page, and select the appropriate schedule from the links there (e.g. "New hire training for technical staff (September 2016)" if that is the most recent).

**If you're arriving alone or with only a few other new hires:** you will work through materials in this library on your own with the help of trainers (listed on the `New hire training <https://confluence.stsci.edu/display/INSTraining/New+hire+training>`_ page) who can answer questions. When the next large cohort of new hires starts, you will join them for the lectures you haven't attended yet. (There is no need to repeat material you already worked through.)

.. admonition:: Exercise
   :class: note
   
   When you have completed your training please provide feedback by completing the short `INS New Hire Training Feedback Survey <https://www.surveymonkey.com/r/X5R3WH5>`_
